ng new jedi-academy --prefix=jad


**WEBPACK**
webpack = biblioteca responsável por criar da aplicação

main.js = scripts da aplicação 
polyfills.js = scripts adicionados para aumentar a compatibilidade com browsers antigos
runtine.js = usado para carregar toda a estrutura para ser carregado no navegador
styles.js = estilos da aplicação,  ex: todos arquivos css
vendor.js = conter scripts de terceiros

**ESTRUTURA ANGULAR**
styles.css = estilos globais da aplicação, está listando no angular-cli.js
polyfills.ts = scripts para adicionar funcionalidades novas
main.ts = responsável por fazer o bootstrap da aplicação
    enableProdMode @angular/core 
    platformBrowserDynamic @angular/platformBrowserDynamic -> responsável por startar a aplicação e setar o bootstrap module, ou seja, o módulo principal da aplicação
    AppMoule - módulo principal que será passado para o plataformBrowserDynamic
    environment -> usado para fazer uma validação se realmente está em modo de produção
AppModule = módulo principal da aplicação
    O decorator @ngModule terá os metadados da aplicação no contexto geral, como declarations, imports, providers e o bootstrap
        declarations - todos os componentes que temos dentro da aplicação
        imports - dependencias que será preciso utilizar 
        providers - 
        bootstrap - quais dos componentes que estão na lista de declarations que será o responsável pelo bootstrap da aplicação

Componentes:

Componentes são pequenas partes reusáveis, são personalizados
Classe/Template/Selector(tag)

decorator @Component (@angular/core)  informado selector e templateURL
ng g c COMPONENT_NAME --skip-specs

Property Binding:
É quando você deseja linkar o valor de uma propriedade de um elemento a uma expressão angular que pode avaliar para propriedade de um componente, método, ou até uma expressão mais elaborada. Sintaxe: [] ao redor da propriedade do elemento, qualquer propriedade de um elemento do DOM.

Sempre que o valor mudar a propriedade do Dom também mudará, mostrando que eles estão ligados (Ony-Way Bindind quando a atualização é sempre em um sentido, componente para o template)

[class.light] = class="light"

Passar os dados para um component através de um component parent
@Input(?String) adicionar ao lado do atributo poderá receber um template interpolation ou um property binding

DIRETIVAS
Adicionam comportamento, porém não tem template

3 tipos:
- componentes
- estruturais
- atributos

<div *diretiva=""></div>

ou

<template [diretiva]="">
    <div></div>
</template>

EVENTOS
Syntax: () entre o evento do dom
ex: <button (click)="clicked()">Click!</button>

pode ser ter uma referencia ao evento do botão através do $event


<input (keydown.space)="keyDown($event)">

Emissão de um evento
Necessário Output(decorator) e EventEmitter do core