# Repositório de estudos
Repositório que visa manter todo material de estudos pessoal sobre minha tragetória na área de desenvolvimento de software, não incluindo os que foram aprendidos através de ensinos superiores ou pós graduações, ou seja, somente de cursos online providos por algumas plataformas.

### Arquitetura

A arquitetura de pastas será constituída pelo conteudo tecnologico que o curso provém, por exemplo, se um curso tem informações sobre três tecnologias diferentes será decomposto entre as pastas que os referenciam. 

Em cada diretório de Tecnologia terá uma serie de anotações de forma geral (general-anotations.md), que não será abordado exclusivamente conteúdo sobre um curso, podendo conter informações sobre terceiros, como documentação, livros, videos, vivência, etc. Além disso, terá todas as pastas que referiarão cada curso, contendo o seu título principal que é disponibilizado pela plataforma que está hospedado.

No interior de cada diretório referente ao curso, terá disponível informações sobre (about.md) e anotações(anotations.md). A organização de pastas dentro irá variar conforme a abordagem disponibilizada, no entendo, em sua maioria irá conter os conteúdos práticos de sua abordagem.

### Cursos

| Nome | Instrutor | Tempo | Abordagem |
|-------|-----------|-------|-----------|
|Entendendo Typescript | Leonardo Moura Leitão | 11h | Tipos, Compilador, Recursos do ES6 em TS, Classes, Namespaces e Modules, Interaces, Generics, Decorators, Integração de libs externas, gulp, webpack, e como utilizar TS em React e Vue|

Project Icon made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>