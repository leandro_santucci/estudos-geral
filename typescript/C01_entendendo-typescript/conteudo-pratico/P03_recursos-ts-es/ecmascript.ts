// let & const
console.log(seraQuePode)
var seraQuePode = '?' //hoisting - variáveis quando são var serão colocadas acima (declaração)

//console.log(seraQuePode2) --> Erro
let seraQuePode2 // Irá dar problema se for o let

/**
 * Variável do var só existe 2 tipos de escopo:
 * Escopo Global no uso geral ou variável local dentro de uma função
 * 
 * Portanto let tem escopo de bloco e não será possível acessar externamente
 */
let estaFrio = true
if(estaFrio) {
    var acao = 'Colocar o casado!'
    let acao2 = 'Colocar moletom'
}
console.log(acao)
//console.log(acao2) --> Erro

/**
 * ES5 não tem constante, então ele irá compilar
 * O Escopo da constante funcionará da mesma maneira que o tipo let 
 */
const cpf: string = '123.456.000-00'
//cpf = '789.101.132-78' --> Erro
console.log(cpf)

/**
 * shadowing
 * 
 */

 var segredo = 'externo!'
 function revelar() {
     var segredo = 'interno'
     console.log(segredo)
 }
 revelar()
 console.log(segredo)


 /**
  * Arrow Function
  */

const subtrair = (n1: number, n2: number): number => n1 - n2

const saudacao = () => console.log("Olá!")

// precisa colocar os parenteses quando será definido o tipo
const falarCom = (pessoa: string) => console.log('Olá' + pessoa)
falarCom('João')

// this

function normalComThis() {
    console.log(this)
}

const normalComThisEspecial = normalComThis.bind(2)
normalComThisEspecial()

// quem é o this aqui neste ponto
console.log(this)
const arrowComThis = () => console.log(this) //window
// na função arrow o this nunca vai variar


/**
 * Parâmetro Padrão
 * 
 */

 function contagemRegressiva(
    inicio: number = 3,
    fim: number = inicio - 2 ): void {
     console.log(inicio)
     while(inicio > fim){
         inicio--
         console.log(inicio)
     }
     console.log("Fim!")
 }

contagemRegressiva()
contagemRegressiva(5)

/**
 * Rest e Spread
 */

const numbers = [1, 10, 99, -5]
console.log(Math.max(numbers[0], numbers[1], numbers[2], numbers[3]))
console.log(Math.max(...numbers)) // Spread / espalhamento

const turmbaA: string[] = ['João', 'Maria', 'Fernanda']
const turmbaB: string[] = ['Fernando', 'Miguel', 'Lorena', ...turmbaA] // Spread
console.log(turmbaB)

function retornarArray(...args: number[]): number[] {
    return args
}

const numeros = retornarArray(1, 2, 3, 4, 5, 345, 623)
console.log(numeros)
console.log(retornarArray(...numbers))


//Rest & Spread (Tupla)
const tupla: [number, string, boolean] = [1, 'abc', false]

function tuplaParam1(a: number, b: string, c: boolean): void {
    console.log(`1) ${a} ${b} ${c}`)
}

tuplaParam1(...tupla)

function tuplaParam2(...params: [number, string, boolean]) {
    console.log(Array.isArray(params))
    console.log(`2) ${params[0]} ${params[1]} ${params[2]}`)
}

tuplaParam2(...tupla)

/**
 * Não é removido, apenas acessado
 * Destructuring (array)
 */
const caracteristicas = ['Motor Zetec 1.8', 2020]
//const motor = caracteristicas[0]
//const ano = caracteristicas[1]

const [motor, ano] = caracteristicas
console.log(motor)
console.log(ano)

/**
 * Destructuring (Object)
 */

const item = {
    nome: 'SSD 480GB',
    preco: 200
}

//const nomeItem = item.nome
//const preco = item.preco

        //alias
const {nome: n, preco} = item

/**
 * Template String
 */

const usuarioID: string = 'SuporteCod3r'
const notificacoes: string = '9'

// const boasVindas = 'Boas vindas' + usuarioID +
//                     'Notificações:' + notificacoes
const boasVindas = `
Boas vindas: ${usuarioID}
Notificações: ${parseInt(notificacoes) > 9 ? '+9' : notificacoes}
` 

console.log(boasVindas)

/**
 * Promises
 */

// Callback
function esperar3s(callback: (dado: string) => void) {
    setTimeout(() => {
        callback('3s depois...')
    }, 3000)
}

esperar3s(function(resultado: string) {
    console.log(resultado)
})

//promise
function esperar3Promise() {
    return new Promise((resolve: any) => {
        setTimeout(() => {
            resolve('3s depois promise...')
        }, 3000)
    }) 
}

esperar3Promise()
    .then(dado => console.log(dado))

fetch('https://swapi.co/api/people/1')
    .then(res => res.json())
    .then(personagem => personagem.films)
    .then(films => fetch(films[0]))
    .then(resFilm => resFilm.json())
    .then(filme => console.log(filme.title))
    .catch(err => console.log('Catch!!!!' + err))