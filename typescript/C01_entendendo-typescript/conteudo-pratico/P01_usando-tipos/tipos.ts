// string
let nome = 'João'
console.log(nome)
//nome = 28 --> Erro de Syntax

// number
let idade = 27
//idade = 'Ana' --> Erro
idade = 27.5
console.log(idade)

// boolean
let possuiHobbies = false
//possuiHobbies = 1 --> Erro

console.log(possuiHobbies)

// tipos explicitos
let minhaIdade: number
minhaIdade = 27
console.log(typeof minhaIdade)
//minhaIdade = 'idade é 27' --> Erro


// array
let hobbies = ['Cozinhar','Praticar Esportes']
console.log(hobbies[0])
console.log(typeof hobbies) // object

//hobbies = [100, 5] --> Erro

// tuplas
let endereco: [string, number] = ["Av Principal", 90]
console.log(endereco)

// enums

enum Cor {
    Cinza, // 0
    Verde = 100, // 100
    Azul, // 101
    Laranja, // 101
    Amarelo, 
    Vermelho = 100
}

let minhaCor: Cor = Cor.Verde
console.log(minhaCor)

console.log(Cor.Azul)
console.log(Cor.Verde, Cor.Vermelho)

// any
//let carro = null assumira como any
let carro: any = 'BMW' 
console.log(carro)
carro = { marca: 'BMW', ano: 2019 }
console.log(carro)



//functions 
function retornaMeuNome(): string {
    //return 1 --> Erro
    return "Leandro"
}

function digaOi(): void {
    console.log("Oi")
    //return minhaIdade --> Erro
}

function multiplicador(numA: number, numB: number): number {
    return numA * numB
}
console.log(multiplicador(5,10))


// tipo função
let calculo: (x: number, y: number) => number
//calculo = digaOi --Erro
//calculo() --> Erro
//calculo = 1 --> Erro

calculo = multiplicador
console.log(calculo(5,6))

// objetos
let usuario = {
    nome: 'João',
    idade: 27
}

// forma explicita
let usuario2: { nome: string, idade: number }

console.log(usuario);
//usuario = {} --> Erro

//usuario = {
//    nome: 'Maria',
//    age: 31
//}
usuario = {
    idade: 21,
    nome: 'Joana'
}
console.log(usuario);

/**
 * Desafio
 * Criar um objeto funcionário com:
 * - Array de strings com os nomes dos supervisores
 * - Função de bater ponto que recebe a hora (numero) e retorna String
 * - Ponto normal (<= 8) e Ponto fora do horário (> 8)
 */

let funcionario: {
    supervisores: string[],
    baterPonto: (horas: number) => string 
} = {
    supervisores: ['Ana', 'Fernando'],
    baterPonto(horario: number) : string {
        return horario <= 8 ? 'Ponto normal' : 'Fora do Horário!'
    }
}

console.log(funcionario.supervisores)
console.log(funcionario.baterPonto)

//funcionario = {} --> Erro 

//Alias
type Funcionario = {
    supervisores: string[],
    baterPonto: (horas: number) => string 
}

let funcionario2: Funcionario = {
    supervisores: ['João', 'Jessica'],
    baterPonto(horario: number) : string {
        return horario <= 8 ? 'Ponto normal' : 'Fora do Horário!'
    }
}

// Union Types
let nota: number | string = 10

console.log(nota)
nota = '10'
console.log(nota)
//nota = true --> Erro


// never
function falha(msg: string): never {
    //while(true){}
    throw new Error(msg)
}

const produto = {
    nome: 'Sabão',
    preco: -1,
    validarProduto() {
        if(!this.nome || this.nome.trim().length == 0 ) {
            falha('Precisa ter um nome')
        }
        if(this.preco <= 0) {
            falha('Preço inválido!')
        }
    }
}

produto.validarProduto()


// valores opcionais

type Lapis = {
    tamanho: number,
    cor: Cor,
    dono?: string
    //dono: null | string (antigamente)
}

const lapis1: Lapis = {
    tamanho: 10,
    cor: Cor.Amarelo
}

const lapis2: Lapis = {
    tamanho: 10,
    cor: Cor.Amarelo,
    dono: 'Leandro'
}


/**
 * Desafio - Transformar codigo JS em TS
 *  let contaBancaria = {
        saldo: 3456,
        depositar(valor) {
            this.saldo += valor
        }
    }
     
    let correntista = {
        nome: 'Ana Silva',
        contaBancaria: contaBancaria,
        contatos: ['34567890', '98765432']
    }
     
    correntista.contaBancaria.depositar(3000)
    console.log(correntista)
 */

type ContaBancaria = {
    saldo: number,
    depositar: (valor: number) => void
}

type Correntista = {
    nome: string,
    contaBancaria: ContaBancaria,
    contatos: string[]
}

const correntista: Correntista = {
    nome: 'Ana Silva',
    contaBancaria: {
        saldo: 0,
        depositar: (val: number) => {
            this.saldo += val
        }
    },
    contatos: ['34567890', '98765432']
}

