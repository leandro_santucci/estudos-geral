// namespace aninhado
namespace Geometria {
    // namespace normal
    export namespace Area {
        export function Retangulo(base: number, altura: number): number {
            return base * altura
        }
    }
}