let canal: string = 'Gaveta'
let inscritos: number = 610234

// com o noEmitOnError == true ele não irá gerar o arquivo com os erros presentes 
// canal = inscritos
console.log(canal);

// você não conseguirá fazer isso porque tudo está dentro de um mesmo escopo, inclusive o nome está presente em teste.ts
// É necessário utilizar modulos e namespaces diferentes para não ocorrer o problema
//let nome: string

// outro contexto
(function() {
    let nome: string = 'Ana'
})()

// function soma(a, b) não dará erro caso o noImplicitAny seja false 
function soma(a: any, b: any) {
    return a + b
}

let qualquerCoisa
qualquerCoisa = 12
qualquerCoisa = 'abc'
// encará com any