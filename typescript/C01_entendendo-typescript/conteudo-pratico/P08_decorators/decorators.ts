//@logarClasse
//@decorator({ 'Teste', 123 })
//@logarClasseSe(true)
@logarObjeto
@imprimivel
class Eletrodomestico {
    constructor() {
        console.log('novo...')
    }
}

function logarClasse(construtor: Function) {
    console.log(construtor)
}

// decorator factory

function decoratorVazio (_: Function) {}

function logarClasseSe(valor: boolean) {
    return valor ? logarClasse : decoratorVazio
}

//factory
function decorator(obj: { a: string, b?: number}) {
    return function(constructor: Function): void {
        console.log(obj.a + '' + obj.b)
    }
}

type Construtor = { new(...args: any[]): {} }

function logarObjeto(construtor: Construtor) {
    console.log('Carregando...')
    return class extends construtor {
        constructor(...args: any[]) {
            console.log('Antes...')
            super(...args)
            console.log('Depois...')
        }
    }
}

interface Eletrodomestico {
    imprimir?(): void
}

//
function imprimivel(construtor: Function) {
    construtor.prototype.imprimir = function() {
        console.log(this)
    }
}

const eletro = new Eletrodomestico()
eletro.imprimir && eletro.imprimir()



const usuarioLogado = {
    nome: 'Guilherme Filho',
    email: 'guigui@gmail.com',
    admin: false
}

@perfilAdmin
class MudancaAdministrativa {
    critico() {
        console.log('Algo crítico foi alterado!')
    }
}

new MudancaAdministrativa().critico()

function perfilAdmin<T extends Construtor>(construtctor: T) {
    return class extends construtctor {
        constructor(...args: any[]) {
            super(...args)
            if(!usuarioLogado || usuarioLogado.admin) {
                throw new Error('Sem permissão!')
            }
        }
    }
}



// Decorator em Métodos
class ContaCorrente {
    @naoNegativo
    private saldo: number
    
    constructor(saldo: number) {
        this.saldo = saldo
    }

    @congelar
    sacar(@paramInfo valor: number) : boolean {
        if (valor <= this.saldo) {
            this.saldo -= valor
            return true
        } else {
            return false
        }
    }

    @congelar
    getSaldo() {
        return this.saldo
    }
}

const cc = new ContaCorrente(10248.57)
cc.sacar(5000)
console.log(cc.getSaldo())

/* cc.getSaldo = function () {
    return this['saldo'] + 7000
}
 */
console.log(cc.getSaldo())


// Object.freeze()
function congelar(alvo: any, nomePropriedade: string,
    descriptor: PropertyDescriptor) {
        console.log(alvo)
        console.log(nomePropriedade)
        descriptor.writable = false // não pode ser sobrescrito
    }

function naoNegativo(alvo: any, nomePropriedade: string) {
    delete alvo[nomePropriedade]
    Object.defineProperty(alvo, nomePropriedade, {
        get: function(): any {
            return alvo["_" + nomePropriedade]
        },
        set: function(valor: any): void {
            if(valor < 0) {
                throw new Error('Saldo Inválido')
            } else {
                alvo["_" + nomePropriedade] = valor
            }
        }
    })
} 

function paramInfo(alvo: any, nomeMetodo: string,
    indiceParam: number) {
        console.log(`Alvo: ${alvo}`)
        console.log(`Método: ${nomeMetodo}`)
        console.log(`Indice Param: ${indiceParam}`)
    }