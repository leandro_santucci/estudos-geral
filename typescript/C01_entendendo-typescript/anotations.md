### Seção 1: Introdução
---
 - O que é typescript?

É uma linguagem baseada em javascript, porém, provém diversos recursos que facilitam uma linguagem mais limpa e organizada e também a encontrar runtime errors. 
Todo conteudo de javascript será entendido por typescript, ou seja, será apenas um acréscimo de diversas novidades em relação ao js.
Quando um recurso novo é acrescentado em javascript, não será recomendado a utilização, pois será necessário aguardar até que todos os browsers o tenham. Portanto, como Typescript é transpilado para uma versão de js no qual é suportada na maioria browsers, poderá utilizar sempre os recursos mais atualizados.

* Instalação
`$ sudo npm install -g typescript`

* Execução
Existem diversas maneiras para execução do código, manual, runner, browser:
    * Manual
    `$ tsc arquivo.ts && arquivo.js`
    * Runner (automatico)
    `$ sudo npm install -g ts-node`
    Instale a extensão chamada *Code Runner* no vscode e após basta utilizar o atalho "ctrl + alt + N"
    * Browser (automatico)
    Criar index.html no projeto npm
    Criar a estrutura padrão html referenciando o javascript que é resultado do código transpilado, por exemplo:
    ```html
    <script src="arquivo.js"></script>
    ```
    Instale o package `live-server` de forma na qual adicione no package.json, adicione o script no start o chamando:
    ```json
    "scripts": {
        "start": "live-server"
    }
    ```
    Basta usar o `npm start` na pasta raiz do projeto e usar o `tsc -w arquivo.ts`

### Seção 2: Usando Tipos

Quando você declara uma variável do tipo let (dinamica), ao atribuir um valor o typescript irá assumir somente como esse tipo, então ao tentar atribuir um valor de outro tipo de dado apresentará um erro. Caso não seja atribuido um valor e não seja declarada explicitamente o Typescript encarará como **any**

> por padão o typescript compila o código mesmo com os erros apresentados, é necessário habilitar para que não transpile os códigos

* Estrutura para declaração explicita:
    * Variáveis:
    `<let, const> <nome da variável>: <tipo>`
    * Funções:
    ```javascript
    function <nome da função>(<param>: <tipo>, <param>?: <tipo>): <tipo retorno> {
        <instrução>
    }
    
    ```
    * Objetos:
    ```
    <let, const> <nome do objeto> = {
        <nome>: tipo,
        <nome>: tipo,
        <nome>?: tipo ---> opcional
    }
    ```
* Enumeradores
São bem conhecidos em algumas linguagens orientadas à objetos, aqui também funcionará da mesma maneira:
```javascript
enum Cor {
    Cinza, //0
    Verde, //1
    Amarelo = 100,
    Azul // 101
    Vermelho = 100
}

```
### Seção 3: Entendendo o Compilador Typescript
Após ser criado o diretório do projeto, poderá utilizar o `tsc --init` para gerar o arquivo de configuração do typescript. Dentro deste, terá diversas opções para o compilador do ts, algumas delas são:

* noEmitOnError

    Quando estiver true evita que gere o arquivo JS mesmo contendo erros.

* target

    Seleção de qual versão do EcmaScript você vai desejar compilar (só irá interferir no código gerado, no qual poderá não suportar em alguns navegadores).

* sourceMap

    Faz a ligação do código gerado ilegível para o source code da aplicação, desta forma poderá depurar com mais facilidade.

* noImplicitAny

    Quando você tenta inicializar uma variável de forma implicita necessáriamente precisa especificar que é do tipi any.

* strictNullChecks

    Mostrará mensagem de erro caso haja possibilidade de retornar nulo em uma função.

* noUnusedParameters

    Parâmetros de funções que não são usados dentro do escopo
    
* noUnusedLocals

    Variável local que não é utilizada

* outDir

    Diretório no qual será gerado os arquivos JS (geralmente em "./build")
    

