### **SOBRE O CURSO**

**Nome:** Entendendo Typescript
**Autor:** Leonardo Moura Leitao
**Link:** https://www.udemy.com/course/typescript-pt/
**Descrição:** Não limite o uso de TypeScript para Angular! Aprenda essa linguagem, seus recursos, fluxos de trabalho e como usá-la!

